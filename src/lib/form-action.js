'use server'

import { redirect } from 'next/navigation'
import { saveMeal } from './meals'
import { revalidatePath } from 'next/cache'

export const handleSubmit = async (preState, formData) => {
  const meal = {
    title: formData.get('title'),
    summary: formData.get('summary'),
    creator_email: formData.get('email'),
    creator: formData.get('name'),
    instructions: formData.get('instructions'),
    image: formData.get('image')
  }

  const { title, summary, creator_email, creator, instructions, image } = meal
  if (
    !title ||
    !summary ||
    !creator_email ||
    !creator ||
    !instructions ||
    !image
  ) {
    return {
      message: 'Invalid input, try again'
    }
  } else {
    await saveMeal(meal)
    revalidatePath('/meals')
    redirect('/meals')
  }
}
