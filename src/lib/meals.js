import sql from 'better-sqlite3'
const db = new sql('meal-food.db')
import slugify from 'slugify'
import xss from 'xss'
import { UploadFileToS3 } from '@/utils/aws/upload'

export async function getMeals() {
  const meals = db.prepare('SELECT * FROM meals')
  await new Promise((resolve) => setTimeout(resolve, 2000))
  return meals.all()
}

export function getDetailMeal(slug) {
  return db.prepare('SELECT * FROM meals WHERE slug = ?').get(slug)
}

export function deleteMeals(id) {
  return db.prepare('DELETE FROM meals WHERE id = ?').run(id)
}

export async function saveMeal(meal) {
  meal.slug = slugify(meal.title, { lower: true })
  meal.instructions = xss(meal.instructions)

  const ext = meal.image.name.split('.').pop()
  const filename = `images/${Date.now()}.${ext}` // save -> folder images in S3
  const bufferImages = await meal.image.arrayBuffer() // convert image to Buffer

  const uploadInfo = await UploadFileToS3({
    filename,
    contentype: meal.image.type,
    chunk: bufferImages
  })

  meal.image = uploadInfo.Location

  db.prepare(
    `
    INSERT INTO meals
      (title, summary, instructions, creator, creator_email, image, slug)
    VALUES (
      @title,
      @summary,
      @instructions,
      @creator,
      @creator_email,
      @image,
      @slug
    )
  `
  ).run(meal)
}
