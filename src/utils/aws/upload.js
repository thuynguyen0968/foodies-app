import { Upload } from '@aws-sdk/lib-storage'
import { S3 } from '@aws-sdk/client-s3'

const s3 = new S3({
  region: process.env.NEXT_PUBLIC_AWS_REGION,
  credentials: {
    accessKeyId: process.env.NEXT_PUBLIC_AWS_ACCESS_KEY,
    secretAccessKey: process.env.NEXT_PUBLIC_AWS_SECRET_KEY
  }
})

export const UploadFileToS3 = async ({
  filename,
  chunk,
  contentype = 'application/octet-stream'
}) => {
  const parallelUploads3 = new Upload({
    client: s3,
    params: {
      Bucket: process.env.NEXT_PUBLIC_AWS_BUCKET, //bucket name
      ContentType: contentype, // stop auto download to use content-type
      Key: filename, // name file after upload
      Body: Buffer.from(chunk) // file need upload in local as buffer
    },

    tags: [], // optional tags
    queueSize: 4, // optional concurrency configuration
    partSize: 1024 * 1024 * 5, // optional size, max is 5MB
    leavePartsOnError: false // optional manually handle dropped parts
  })
  return parallelUploads3.done()
}
