'use client'
import { AppProgressBar as ProgressBar } from 'next-nprogress-bar'

export default function Providers({ children }) {
  return (
    <>
      <ProgressBar
        height='4px'
        color='#fff'
        options={{ showSpinner: false }}
        shallowRouting
      />
      {children}
    </>
  )
}
