import Image from 'next/image'
import { notFound } from 'next/navigation'

import { getDetailMeal } from '@/lib/meals'
import classes from './page.module.css'

export async function generateMetadata({ params }) {
  const { slug } = params

  // fetch data
  const meal = getDetailMeal(slug)

  return {
    title: `${meal.title} | ${meal.creator}`,
    description: meal.summary
  }
}

export default function MealDetailsPage({ params }) {
  const meal = getDetailMeal(params.slug)
  if (!meal) {
    notFound()
  }

  meal.instructions = meal.instructions.replace(/\n/g, '<br />')

  return (
    <>
      <header className={classes.header}>
        <div className={classes.image}>
          <Image
            src={meal.image}
            alt={meal.title}
            fill
            sizes='(min-width: 808px) 50vw, 75vw'
          />
        </div>
        <div className={classes.headerText}>
          <h1>{meal.title}</h1>
          <p className={classes.creator}>
            by <a href={`mailto:${meal.creator_email}`}>{meal.creator}</a>
          </p>
          <p className={classes.summary}>{meal.summary}</p>
        </div>
      </header>
      <main>
        <p
          className={classes.instructions}
          dangerouslySetInnerHTML={{
            __html: meal.instructions
          }}
        ></p>
      </main>
    </>
  )
}
