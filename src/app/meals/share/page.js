'use client'

import { ButtonSubmit, ImagePicker } from '@/components'
import classes from './page.module.css'
import { handleSubmit } from '@/lib/form-action'
import { useFormState } from 'react-dom'

export default function ShareMealPage() {
  const [state, formAction] = useFormState(handleSubmit, { message: null })
  return (
    <>
      <header className={classes.header}>
        <h1>
          Share your <span className={classes.highlight}>favorite meal</span>
        </h1>
        <p>Or any other meal you feel needs sharing!</p>
      </header>
      <main className={classes.main}>
        <form className={classes.form} autoComplete='off' action={formAction}>
          <div className={classes.row}>
            <p>
              <label htmlFor='name'>Your name</label>
              <input id='name' name='name' />
            </p>
            <p>
              <label htmlFor='email'>Your email</label>
              <input id='email' name='email' />
            </p>
          </div>
          <p>
            <label htmlFor='title'>Title</label>
            <input id='title' name='title' />
          </p>
          <p>
            <label htmlFor='summary'>Short Summary</label>
            <input id='summary' name='summary' />
          </p>
          <p>
            <label htmlFor='instructions'>Instructions</label>
            <textarea
              id='instructions'
              name='instructions'
              rows='10'
            ></textarea>
          </p>
          <ImagePicker label='Your image' name='image' />
          {state.message && <p className={classes.error}>{state.message}</p>}
          <p className={classes.actions}>
            <ButtonSubmit />
          </p>
        </form>
      </main>
    </>
  )
}
