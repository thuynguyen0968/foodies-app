import Link from 'next/link'
import classes from './not-found.module.css'

export const metadata = {
  title: 'Not found page or resouces',
  description:
    'Unfortunately, we could not find the requested page or resource.'
}

export default function NotFound() {
  return (
    <main className='not-found'>
      <h1>Not found page</h1>
      <p>{metadata.description}</p>

      <div className={classes.cta}>
        <Link href='/'>Back to Home Page</Link>
      </div>
    </main>
  )
}
