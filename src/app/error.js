'use client'

export const metadata = {
  title: 'Error system',
  description: 'Something was broken, try again later'
}

export default function Error() {
  return (
    <main className='error'>
      <h1>An error occurred</h1>
      <p>{metadata.description}</p>
    </main>
  )
}
