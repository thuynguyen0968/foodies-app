import { MainHeader } from '@/components'
import Providers from '@/providers'
import './globals.css'
import { Poppins } from 'next/font/google'

const popins = Poppins({
  subsets: ['latin'],
  display: 'swap',
  weight: ['400', '500', '600', '700']
})

export const metadata = {
  title: 'Foodies app',
  description: 'Delicious meals, shared by a food-loving community.'
}

export default function RootLayout({ children }) {
  return (
    <html lang='en' className={popins.className}>
      <body>
        <MainHeader />
        <Providers>{children}</Providers>
      </body>
    </html>
  )
}
