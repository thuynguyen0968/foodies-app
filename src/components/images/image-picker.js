'use client'

import { useRef, useState } from 'react'
import classes from './image-picker.module.css'
import Image from 'next/image'

export function ImagePicker({ label, name }) {
  const [pickerImg, setPickerImg] = useState(null)
  const imageInput = useRef()

  function handlePickClick() {
    imageInput.current.click()
  }

  const handleChange = (e) => {
    const file = e.target.files[0]

    if (!file) {
      setPickerImg(null)
      return
    }
    const imageURL = URL.createObjectURL(file)
    setPickerImg(imageURL)
  }

  return (
    <div className={classes.picker}>
      <label htmlFor={name}>{label}</label>
      <div className={classes.controls}>
        <div className={classes.preview}>
          {pickerImg ? (
            <Image
              src={pickerImg}
              alt='The image selected by the user.'
              width={200}
              height={200}
            />
          ) : (
            <p>No image picked yet.</p>
          )}
        </div>
        <input
          className={classes.input}
          type='file'
          id={name}
          accept='image/png, image/jpeg'
          name={name}
          ref={imageInput}
          onChange={handleChange}
        />
        <button
          className={classes.button}
          type='button'
          onClick={handlePickClick}
        >
          Pick an Image
        </button>
      </div>
    </div>
  )
}
