'use client'
import { useFormStatus } from 'react-dom'

export function ButtonSubmit() {
  const { pending } = useFormStatus()
  return (
    <button disabled={pending} type='submit'>
      {pending ? 'Sharing meal...' : 'Share Meals'}
    </button>
  )
}
