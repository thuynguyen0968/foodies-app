import Link from 'next/link'
import logoImg from '@/assets/logo.png'
import classes from './headers.module.css'
import Image from 'next/image'
import { HeaderBg } from './headerBg'
import NavLink from './navLink'

const navLinks = [
  { id: 1, href: '/meals', title: 'Meals' },
  { id: 2, href: '/community', title: 'Community' }
]

export function MainHeader() {
  return (
    <>
      <HeaderBg></HeaderBg>
      <header className={classes.header}>
        <Link className={classes.logo} href='/'>
          <Image
            width={80}
            height={80}
            src={logoImg}
            alt='A plate with food on it'
            priority
          />
          NextLevel Food
        </Link>

        <nav className={classes.nav}>
          <ul>
            {navLinks.map((link) => (
              <li key={link.id}>
                <NavLink href={link.href}>{link.title}</NavLink>
              </li>
            ))}
          </ul>
        </nav>
      </header>
    </>
  )
}
