'use client'

import Link from 'next/link'
import { usePathname } from 'next/navigation'
import classes from './nav.module.css'

export default function NavLink({ href, children }) {
  const pathName = usePathname()
  return (
    <Link
      className={
        href === pathName || pathName.includes(href) ? classes.active : ''
      }
      href={href}
    >
      {children}
    </Link>
  )
}
